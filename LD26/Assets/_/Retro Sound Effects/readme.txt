Features

A big collection of 8Bit video game inspired sound effects.
Contains 81 unique sounds

Feel free to contact me trough my website at http://www.chemicalbliss.com/
Sound testing for each audio asset with pitch can be found at http://chemicalbliss.com/wordpress/portfolio-items/8bit-game-sound-effects/

Thanks for purchasing this asset
Have fun with Unity3D :)