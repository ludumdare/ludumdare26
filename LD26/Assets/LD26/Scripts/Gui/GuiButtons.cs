using UnityEngine;
using System.Collections;
using EleckTek;

public class GuiButtons : MonoBehaviour {
	
	void Start() {
		DontDestroyOnLoad(gameObject);
	}
	
	public void StartGame() {
		
		D.log("Start Game");
		gameObject.SendMessageUpwards("Hide", SendMessageOptions.DontRequireReceiver);
		GameController.Instance.StartGame();
	}
	
	public void NextLevel() {
		
		D.log("Next Level");
		gameObject.SendMessageUpwards("Hide", SendMessageOptions.DontRequireReceiver);
		GameController.Instance.StartLevel();
	}
	
	public void PauseGame() {
		D.log("Pause Game");
		GameObject.Find("GuiDialogPause").SendMessage("Show");
		PauseController _pause = (PauseController) GameObject.Find("PauseController").GetComponentInChildren<PauseController>();
		_pause.ActivatePauseProtocol();
	}
	
	public void PlayGame() {
		D.log("Play Game");
		GameObject.Find("GuiDialogPause").SendMessage("Hide");
		PauseController _pause = (PauseController) GameObject.Find("PauseController").GetComponentInChildren<PauseController>();
		_pause.DeactivatePauseProtocol();
		
	}
	
	public void GoHome() {
		D.log("Go Home");
		GameController.Instance.KillGame();
		gameObject.SendMessageUpwards("Hide", SendMessageOptions.DontRequireReceiver);
		GameObject.Find("GuiDialogStart").SendMessage("Show");
		PauseController _pause = (PauseController) GameObject.Find("PauseController").GetComponentInChildren<PauseController>();
		_pause.DeactivatePauseProtocol();
		
	}
	
}
