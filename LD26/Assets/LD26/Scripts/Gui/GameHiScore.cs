using UnityEngine;
using System.Collections;

public class GameHiScore : MonoSingleton<GameHiScore> {

	private tk2dTextMesh 		m_text;
	
	// Use this for initialization
	void Start () {
		
		m_text = (tk2dTextMesh) gameObject.GetComponentInChildren<tk2dTextMesh>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void UpdateScore() {
		m_text.text = GameController.Instance.Global.HiScore.ToString();
		m_text.Commit();
	}
}
