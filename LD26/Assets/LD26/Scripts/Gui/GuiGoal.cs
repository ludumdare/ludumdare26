using UnityEngine;
using System.Collections;

public class GuiGoal : MonoSingleton<GuiGoal> {

	private tk2dTextMesh 		m_text;
	
	// Use this for initialization
	void Start () {
		
		m_text = (tk2dTextMesh) gameObject.GetComponentInChildren<tk2dTextMesh>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void UpdateGoal() {
		m_text.text = GameController.Instance.World.WorldCompleted + "%";
		m_text.Commit();
	}
}
