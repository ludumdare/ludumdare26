using UnityEngine;
using System.Collections;

public class GameLives : MonoSingleton<GameLives> {

	private tk2dTextMesh 		m_text;
	
	// Use this for initialization
	void Start () {
		
		m_text = (tk2dTextMesh) gameObject.GetComponentInChildren<tk2dTextMesh>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void UpdateLives() {
		m_text.text = "LIVES : " + GameController.Instance.Global.Lives.ToString();
		m_text.Commit();
	}
}
