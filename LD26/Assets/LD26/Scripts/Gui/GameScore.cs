using UnityEngine;
using System.Collections;

public class GameScore : MonoSingleton<GameScore> {
	
	private tk2dTextMesh 		m_text;
	
	// Use this for initialization
	void Start () {
		
		m_text = (tk2dTextMesh) gameObject.GetComponentInChildren<tk2dTextMesh>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void SetScore(int score) {
		m_text.text = score.ToString();
		m_text.Commit();
		
		if ( score > GameController.Instance.Global.HiScore ) {
		
			GameController.Instance.Global.HiScore = score;
			GameHiScore.Instance.UpdateScore();
			PlayerPrefs.SetInt("hiscore", score);

		}
		
	}
}
