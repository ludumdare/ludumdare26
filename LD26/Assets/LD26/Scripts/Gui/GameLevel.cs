using UnityEngine;
using System.Collections;

public class GameLevel : MonoSingleton<GameLevel> {

	private tk2dTextMesh 		m_text;
	
	// Use this for initialization
	void Start () {
		
		m_text = (tk2dTextMesh) gameObject.GetComponentInChildren<tk2dTextMesh>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void UpdateValue() {
		m_text.text = GameController.Instance.Global.Level.ToString();
		m_text.Commit();
	}
}
