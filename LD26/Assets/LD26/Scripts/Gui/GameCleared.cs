using UnityEngine;
using System.Collections;

public class GameCleared : MonoSingleton<GameCleared> {

	private tk2dTextMesh 		m_text;
	private GameGlobal			m_global;
	
	// Use this for initialization
	void Awake () {
		
		m_text = (tk2dTextMesh) gameObject.GetComponentInChildren<tk2dTextMesh>();
		m_global = GameController.Instance.Global;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void SetCleared(int cleared) {
		int _clear = Mathf.RoundToInt (  ( (float) cleared / (float) GameController.Instance.World.WorldArea ) * 100 );
		
		// has the world been cleared?
		
		//D.log("clear = " + _clear);
		//D.log("clear goal = " + GameController.Instance.World.WorldCleared);
		if ( _clear > GameController.Instance.World.WorldCleared ) {
			GameController.Instance.World.WorldShowExit = true;
		}
		
		m_text.text = _clear.ToString() + "% of " + GameController.Instance.World.WorldCleared.ToString() + "%";
		m_text.Commit();
	}
}
