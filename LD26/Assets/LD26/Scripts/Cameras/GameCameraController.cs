using UnityEngine;
using System.Collections;

public class GameCameraController : MonoBehaviour {
	
	public GameObject Target;
	public float 	Smooth;
	public float 	OffsetX;
	public float 	OffsetY;
	public float	MinX;
	public float	MaxX;
	public float 	MinY;
	public float 	MaxY;
	
	// Use this for initialization
	public void Init() {
	
		//MinX = ( ( ( GameController.Instance.World.WorldWidth / 2 ) - 1 ) * 64) + OffsetX;
		//MinY = ( ( ( GameController.Instance.World.WorldDepth / 2 ) - 1 ) * 64) + OffsetY;
		
		//MaxX = MinX;
		//MaxY = MinY;
	}
	
	// Update is called once per frame
	void Update () {
		
		if ( Target == null ) return;
	
		float _x = Target.transform.position.x + OffsetX;
		float _y = Target.transform.position.y + OffsetY;
		float _z = transform.position.z;
		
		//_x = Mathf.Clamp( _x, MinX, MaxX );
		//_y = Mathf.Clamp( _y, MinY, MaxY );
		
		//Vector3 _dest = new Vector3(transform.position.x, Target.transform.position.y + OffsetY, transform.position.z);
		Vector3 _dest = new Vector3( _x , _y , _z );
		Vector3 position = Vector3.Lerp(transform.position, _dest, Time.deltaTime * Smooth);
    	transform.position = position;	
		
	}
	
	
}
