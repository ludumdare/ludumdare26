using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class GameSprite : MonoBehaviour {
	
	public float			PosX;
	public float			PosY;
	
	private GameWorld		m_world;
	private float			m_xpos;
	private float			m_ypos;
	private float			m_base = 64f;
	private float			m_ymax;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void MoveLeft(int step) {
		if ( m_xpos <= 0 ) return;
		m_xpos = m_xpos - 1;
		_Move();
	}
	
	public void MoveRight(int step) {
		if ( m_xpos >= ( m_world.WorldWidth - 1 ) ) return;
		m_xpos = m_xpos + 1;
		_Move();
	}
	
	public void MoveDown(int step) {
		if ( m_ypos <= 0 ) return;
		m_ypos = m_ypos - 1;
		_Move();
	}
	
	public void MoveUp(int step) {
		if ( m_ypos >= ( m_world.WorldDepth - 1 ) ) return;
		m_ypos = m_ypos + 1;
		_Move();
	}
	
	
	// Move the sprite on the game grid, not
	// actual vector coordinates
	
	public void MoveTo( Vector2 vector ) {
		
		m_xpos = vector.x;
		m_ypos = vector.y;
		
		_Move();		
	}
	
	private void _Move() {
		
		m_world = GameController.Instance.World;
		m_ymax = m_world.WorldDepth * 5;

		PosX = m_xpos;
		PosY = m_ypos;
		
		float _x = ( m_xpos * m_base );
		float _y = ( m_ypos * m_base );
		
		float _z = ( m_ypos * 5f ) - ( m_ymax );
		
		Vector3 _vector = new Vector3( _x, _y, _z );
		//iTween.MoveTo(gameObject, _vector, 1);
		//HOTween.To( transform, 1, new TweenParms().Prop("position", _vector) );
		transform.position = _vector;
		SendMessage("MoveComplete", SendMessageOptions.DontRequireReceiver);
	}
	
	public int x {
		get { return (int) m_xpos; }
	}

	public int y {
		get { return (int) m_ypos; }
	}

}
