using UnityEngine;
using System.Collections;

public class GamePowerupController : MonoSingleton<GamePowerupController> {
	
	public int					MaxPowerups;
	public float				SpawnDelay;
	public float				SpawnAlive;
	public GameObject[]			Powerups;
	
	private  bool					m_running;
	private  bool					m_delay;
	

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		//D.log("running=" + m_running);
		//D.log("delay="+m_delay);
		
		if ( ! m_running ) return;
		
		if ( m_delay ) return;
		
		StartCoroutine(Run());
	}
	
	public void Init( int max, int delay, int alive) {
		
		MaxPowerups = max;
		SpawnDelay = delay;
		SpawnAlive = alive;
	}
	
	public void StartRunning() {
		
		m_running = true;		
	}
	
	private IEnumerator Run() {
	
		//D.log("spawndelay="+SpawnDelay);
		
		m_delay = true;
		
		yield return new WaitForSeconds(SpawnDelay);
		
		for ( int i = 0; i < MaxPowerups; i++ ) {
			int _pick = Random.Range(0,Powerups.Length);
			
			GameObject _go = (GameObject) Object.Instantiate( Powerups[_pick] );
			D.log("Trying to spawn " + _go.name);
			GamePowerup _gpu = (GamePowerup) _go.GetComponentInChildren<GamePowerup>();
			
			int _rand = Random.Range(0, _gpu.RandomWeight);
			
			// if we hit weighted randome of 0
			
			if ( _rand == 0 ) {
				_go.transform.parent = GameObject.Find("_World").transform;
				GameController.Instance.MoveToRandom(_go,true);
			}
			else {
				Destroy(_go);
			}
		}
		
		m_delay = false;
		
	}
	
	public void StopRunning() {
		
		m_running = false;
		StopAllCoroutines();
	}
	
	
}
