using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class GamePowerup : MonoBehaviour {
	
	public string		Name;
	public string		Action;
	public int			Value;
	public AudioClip	Sound;
	public int 			RandomWeight;

	void OnTriggerEnter(Collider other) {
	
		if ( other.name == "Enemy" ) {
			
			BoxCollider _box = (BoxCollider) gameObject.GetComponentInChildren<BoxCollider>();
			_box.isTrigger = false;
			
			Destroy(transform.Find("PowerupSprite").gameObject);		
			
			// get particle
			
			ParticleSystem _part = (ParticleSystem) gameObject.GetComponentInChildren<ParticleSystem>();
			_part.transform.parent = GameObject.Find("_World").transform;
			_part.Stop();
			_part.Play();
			
						
		}
		
		if ( other.name == "Player" ) {
			
			BoxCollider _box = (BoxCollider) gameObject.GetComponentInChildren<BoxCollider>();
			_box.isTrigger = false;
			
			audio.clip = Sound;
			audio.Play();
			
			// get particle
			
			TweenParms parms = new TweenParms();
			
			parms.Prop("position", new Vector3(0,150,-100), true); // Position tween
			parms.Prop("rotation", new Vector3(0,0,720), true); // Rotation tween
			parms.Prop("localScale", new Vector3(3f,3f,3f)); // Scale tween
			parms.Ease(EaseType.Linear); // Easing type
			parms.OnComplete(EndMove);
			parms.Loops(1);
			
			HOTween.To(gameObject.transform, 2, parms );	
			
		}
		
	}		
	
	void EndMove() {
		
		if ( Action == "Score" ) {
			
			GameController.Instance.AddScore(Value);
		}
		
		if ( Action == "Kill" ) {
		
			GameObject _enemy =  GameObject.Find("Enemy");
			if ( _enemy != null ) {
				Destroy(_enemy);
				GameObject.Find("GuiDialogKillEnemy").SendMessage("AutoShow");
			}
		}
		
		if ( Action == "Life" ) {
			GameController.Instance.Global.Lives++;
			GameLives.Instance.UpdateLives();
			GameObject.Find("GuiDialogAddLife").SendMessage("AutoShow");
		}
		
		Destroy(gameObject,0);
		
	}
}
