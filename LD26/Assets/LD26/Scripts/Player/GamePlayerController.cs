using UnityEngine;
using System.Collections;

public class GamePlayerController : MonoSingleton<GamePlayerController> {
	
	public float			KeyDelay;
	
	private GameObject		m_player;
	private GameSprite		m_sprite;
	private bool			m_delay = false;
	private bool			m_kill = false;
	private bool			m_disable = false;
	
	// Use this for initialization
	void Awake () {
		DontDestroyOnLoad(gameObject);
	}
	
	// Update is called once per frame
	
	void Update () {
		
		//D.log("disable=" + m_disable);
		
		if (m_kill) {
			
			Destroy(m_player);
			return;
		}
		
		if ( m_disable ) return;
		
		if ( m_delay ) return;
		
		// capture keypress
		
		if ( Input.GetKey ( KeyCode.LeftArrow) || Input.GetKey ( KeyCode.A)) {
			if ( MoveLeft() )  return;
		}
		
		if ( Input.GetKey ( KeyCode.RightArrow ) || Input.GetKey ( KeyCode.D) ) {
			if ( MoveRight() )  return;
		}
		
		if ( Input.GetKey ( KeyCode.UpArrow )  || Input.GetKey ( KeyCode.W)) {
			if ( MoveUp() )  return;
		}
		
		if ( Input.GetKey ( KeyCode.DownArrow ) || Input.GetKey ( KeyCode.S) ) {
			if ( MoveDown() )  return;
		}
		
	}
	
	private bool MoveLeft() {
		
		if ( !GameController.Instance.World.IsPassableTerrain( m_sprite.x - 1, m_sprite.y ) ) 
			{ return false; }
		
		m_sprite.MoveLeft(1);
		StartCoroutine(DoDelay());
		return true;
		
		return false;
	}
	
	private bool MoveRight() {
		
		if ( !GameController.Instance.World.IsPassableTerrain( m_sprite.x + 1, m_sprite.y ) ) 
			{ return false; }
		
		m_sprite.MoveRight(1);
		StartCoroutine(DoDelay());
		return true;
		
		return false;
	}
	
	private bool MoveUp() {
		
		if ( !GameController.Instance.World.IsPassableTerrain( m_sprite.x , m_sprite.y + 1 ) ) 
			{ return false; }
		
		m_sprite.MoveUp(1);
		StartCoroutine(DoDelay());
		return true;
		
		return false;
	}
	
	private bool MoveDown() {
		
		if ( !GameController.Instance.World.IsPassableTerrain( m_sprite.x, m_sprite.y - 1 ) ) 
			{ return false; }
		
		m_sprite.MoveDown(1);
		StartCoroutine(DoDelay());
		return true;
		
		return false;
	}
	
	private IEnumerator DoDelay() {
		
		m_delay = true;
		yield return new WaitForSeconds(KeyDelay);
		m_delay = false;
	}
	
	public void CreatePlayer() {
		
		m_kill = false;	
		m_delay = false;
		m_disable = false;
		
		// get the player prefab & create it
		
		string _prefab = "Prefabs/Player/Player";
		m_player = ( GameObject ) Object.Instantiate( Resources.Load( _prefab ) );
		m_player.name = "Player";
		m_sprite = ( GameSprite ) m_player.GetComponentInChildren<GameSprite>();
		
		// attach player to World object
		
		GameObject _world = GameObject.Find("_World");
		m_player.transform.parent = _world.transform;
		
		// set the camera target to my new player
		GameObject _camera = GameObject.Find("GameCamera");
		GameCameraController _cc = ( GameCameraController) _camera.GetComponentInChildren<GameCameraController>();
		_cc.Target = m_player;
		
		// move player to a random position on the playfield
		
		int _x;
		int _y;
		bool _ok;
		
		do {
			
			_ok = true;
			
			_x = Random.Range( 0, GameController.Instance.World.WorldWidth );
			_y = Random.Range( 0, GameController.Instance.World.WorldDepth );
			
			if ( GameController.Instance.World.IsMinimalTerrain(_x,_y) ) {
				_ok = false;
			}
			
			if ( ! GameController.Instance.World.IsPassableTerrain(_x,_y) ) {
				_ok = false;
			}
			
		} while ( ! _ok );
		
		// convert to world position ( base 64 )
		
		float _px = ( _x * 64 );
		float _py = ( _y * 64 );
		float _pz = ( _y * -1 );
		
		//m_player.transform.position = new Vector3( _px, _py, _pz );
		
		Vector2 _vector = new Vector3( _x, _y );
		m_sprite.MoveTo( _vector );
		
	}
	
	public void Disable() {
		m_disable = true;
	}
	
	public void Kill() {
		m_disable = true;
		Destroy(m_player);
	}
	
}
