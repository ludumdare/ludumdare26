using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class GamePlayer : MonoBehaviour {
	
	private GameSprite		m_sprite;
	
	public AudioClip		Movement;
	
	void Awake() {
		m_sprite = (GameSprite) gameObject.GetComponentInChildren<GameSprite>();
	}
	
	void Update() {
		
	}
	
	public void MoveComplete() {
		
		//D.log("GamePlayer:MoveComplete");
		audio.clip = Movement;
		audio.Play();
		
		GameWorldTerrain _terrain = GameController.Instance.World.GetWorldTerrain( m_sprite.x, m_sprite.y );
		
		if ( _terrain.IsMinimal ) {
			GameController.Instance.DestroyMinimal(m_sprite.x, m_sprite.y);
			//GameController.Instance.AddCleared(1);
		}
	}
	
	void OnTriggerEnter(Collider other) {
	
		if ( other.name == "Enemy" ) {
			
			// get particle
			
			ParticleSystem _part = (ParticleSystem) gameObject.GetComponentInChildren<ParticleSystem>();
			_part.transform.parent = GameObject.Find("_World").transform;
			_part.Stop();
			_part.Play();
			
			D.log("Hit the Enemy");
			
			GamePlayerController.Instance.Disable();
			
			TweenParms parms = new TweenParms();
			
			parms.Prop("position", new Vector3(0,700,-100), true); // Position tween
			parms.Prop("rotation", new Vector3(0,0,720), true); // Rotation tween
			parms.Prop("localScale", new Vector3(3f,3f,3f)); // Scale tween
			parms.Ease(EaseType.Linear); // Easing type
			parms.OnComplete(RestartPlayer);
			parms.Loops(1);
			
			HOTween.To(gameObject.transform, 2, parms );	
			
		}
	}

	void RestartPlayer() {
		
		GameController.Instance.Global.Lives--;
		GameLives.Instance.UpdateLives();
		
		if ( GameController.Instance.Global.Lives < 1 ) {
			GameController.Instance.GameOver();
			return;
		}
		
		GamePlayerController.Instance.Kill();
		GamePlayerController.Instance.CreatePlayer();
		
	}
	
}
