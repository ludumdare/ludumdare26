using UnityEngine;
using System.Collections;

public class GameEnemy : MonoBehaviour {
	
	public float			DelayTime;

	private GameSprite		m_sprite;
	private bool			m_delay;
	private bool			m_disabled;
	
	void Awake() {
		m_sprite = (GameSprite) gameObject.GetComponentInChildren<GameSprite>();
	}
	
	void Update() {
		
		if ( m_disabled ) return;
		
		if ( m_delay ) return;
		
		int _move = Random.Range(0,4);
		
		//D.log("Enemy Move");
		
		switch ( _move ) {
		
		case 0 : 		// left
			if ( GameController.Instance.World.IsPassableTerrain( m_sprite.x - 1, m_sprite.y ) ) 
			m_sprite.MoveLeft(1);
			break;
		case 1 :		// right
			if ( GameController.Instance.World.IsPassableTerrain( m_sprite.x + 1, m_sprite.y ) ) 
			m_sprite.MoveRight(1);
			break;
			
		case 2 :		// up
			if ( GameController.Instance.World.IsPassableTerrain( m_sprite.x, m_sprite.y + 1 ) ) 
			m_sprite.MoveUp(1);
			break;
			
		case 3 :		// down
			if ( GameController.Instance.World.IsPassableTerrain( m_sprite.x, m_sprite.y - 1 ) ) 
			m_sprite.MoveDown(1);
			break;
				
		}
		
		StartCoroutine(MoveDelay());	
		
	}
	
	private IEnumerator MoveDelay() {
		m_delay = true;
		yield return new WaitForSeconds(DelayTime);
		m_delay = false;
	}
	
	public void MoveTo(Vector2 vector) {
		m_sprite.MoveTo(vector);
	}
	
	public void MoveComplete() {
		//D.log("GameEnemy:MoveComplete");
		GameWorldTerrain _terrain = GameController.Instance.World.GetWorldTerrain( m_sprite.x, m_sprite.y );
		
		if ( ! _terrain.IsMinimal ) {
			GameController.Instance.MakeMinimal(m_sprite.x, m_sprite.y);
			//GameController.Instance.AddCleared(-1);
		}
	}
	
	public void SetDelayTime(float sec) {
		D.log("SetDelayTime");
		DelayTime = sec;	
	}
	
	public void SetDisabled(bool disabled) {
		D.log("SetDisabled");
		m_disabled = disabled;
	}
	
}
