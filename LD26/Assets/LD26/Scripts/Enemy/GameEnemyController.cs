using UnityEngine;
using System.Collections;

public class GameEnemyController : MonoSingleton<GameEnemyController> {
	
	void Awake () {
		DontDestroyOnLoad(gameObject);
	}
	
	public void CreateEnemy( float speed) {
		
		// get the player prefab & create it
		
		string _prefab = "Prefabs/Enemy/Enemy";
		GameObject m_enemy = ( GameObject ) Object.Instantiate( Resources.Load( _prefab ) );
		m_enemy.name = "Enemy";
		
		// attach player to World object
		
		GameObject _world = GameObject.Find("_World");
		m_enemy.transform.parent = _world.transform;
		
		// move enemy to a random position on the playfield
		
		int _x;
		int _y;
		bool _ok;
		
		do {
			
			_ok = false;
			
			_x = Random.Range( 0, GameController.Instance.World.WorldWidth );
			_y = Random.Range( 0, GameController.Instance.World.WorldDepth );
			
			if ( GameController.Instance.World.IsMinimalTerrain(_x,_y) ) {
				_ok = true;
			}
			
		} while ( ! _ok );
		
		// convert to world position ( base 64 )
		
		float _px = ( _x * 64 );
		float _py = ( _y * 64 );
		float _pz = ( _y * -1 );
		
		//m_player.transform.position = new Vector3( _px, _py, _pz );
		
		Vector2 _vector = new Vector3( _x, _y );
		m_enemy.SendMessage( "MoveTo", _vector );
		
		m_enemy.SendMessage("SetDelayTime", speed);
		m_enemy.SendMessage("SetDisabled", false);
	}
}
