using UnityEngine;
using System.Collections;

/**
 * 
 * World terrain define how players interact with the environment.
 * 
 */

public class GameWorldTerrain {

	public bool			IsMinimal = false;	// has this been minimalised?
	public bool			IsPassable = true;	// is this passable?
	public GameObject	TerrainObject; 		// the go for the terrain
	public string		TerrainsId;			// id of the tile to display
	public GameObject	SceneryObject;		// scenery object
	public string		SceneryType;		// type of scenery
	public string		SceneryId;			// id of the scenery tile
}
