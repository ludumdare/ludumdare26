using UnityEngine;
using System.Collections;

/**
 * Holds properties of a world.
 * 
 * Normally we would make all props private and
 * expose thru getter/setters, but for the sake
 * of time in LD, we are exposing those we need
 * as public.
 * 
 */

public class GameWorld {
	
	public int			WorldWidth;			// how wide
	public int			WorldDepth;			// how deep
	public int			WorldScenery;		// estimated scenery count
	public int			WorldEnemies;		// how many enemies might show up
	public int			WorldEnemyMax;		// maximum enemies in game at one time
	public int			WorldAnimals;		// how many animals might show up
	public int			WorldAnimalMax;		// maximum animals in game at once
	public int			WorldValue;			// how many coins might show up ( higher levels = more )
	public int			WorldPower;			// how many powerups might show up ( higher levels = more )
	public int			WorldArea;			// total area to cover ( minus impassables )
	public int			WorldCleared;		// total % to be cleared for portal ( base is 80% )
	public bool			WorldCompleted;		// has the player completed the world?
	public bool			WorldShowExit;	// player has cleared enuf to show exit portal
	
	public GameWorldTerrain[,]		GameWorldTerrains;
	
	public GameWorldTerrain GetWorldTerrain( int x, int y ) {
		if ( ! CheckBounds(x,y) ) return null;
		return GameWorldTerrains[x,y];
	}
	
	public bool IsMinimalTerrain( int x, int y) {
		if ( ! CheckBounds(x,y) ) return false;
		return GameWorldTerrains[x,y].IsMinimal;
	}
	
	public bool IsPassableTerrain( int x, int y) {
		if ( ! CheckBounds(x,y) ) return false;
		return GameWorldTerrains[x,y].IsPassable;
	}
	
	public void SetMinimal(int x, int y, bool val) {
		if ( ! CheckBounds(x,y) ) return;
		GameWorldTerrains[x,y].IsMinimal = val;		
	}
	
	public void SetPassable(int x, int y, bool val) {
		if ( ! CheckBounds(x,y) ) return;
		GameWorldTerrains[x,y].IsPassable = val;		
	}
	
	public GameObject GetTerrainObject(int x, int y) {
		if ( ! CheckBounds(x,y) ) return null;
		return GameWorldTerrains[x,y].TerrainObject;
		
	}
	
	private bool CheckBounds(int x, int y) {
		
		//D.log("width=" + WorldWidth);
		//D.log("depth=" + WorldDepth);
		//D.log("x=" + x);
		//D.log("y=" + y);
		
		if ( x < 0 ) return false;
		if ( x > WorldWidth - 1) return false;
		if ( y < 0 ) return false;
		if ( y > WorldDepth - 1) return false;
		
		return true;
	}
	
}
