using UnityEngine;
using System.Collections;

/**
 * 
 * Global variables for the Game.
 * 
 * Again we are publicly exposing vars here to save time.
 * 
 */

public class GameGlobal {
	
	public int			Score;
	public int			HiScore;
	public int			Lives;
	public string		Username;
	public string		Password;
	public int			Level;
	public int			Stage;
	public int			Cleared;
	public int			Difficulty;
	
}
