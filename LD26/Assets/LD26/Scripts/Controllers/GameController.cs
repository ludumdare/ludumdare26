using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class GameController : MonoSingleton<GameController> {
	
	public GameObject		GameCamera;
	public int				StartingLevel=0;
	public int				StartingDifficulty=1;
	
	
	private GameWorld		m_world;
	private GameGlobal		m_global;
	private bool			m_levelrunning;
	
	// Use this for initialization
	
	void Start() {
		
		DontDestroyOnLoad(gameObject);
	}
	
	void Update() {
		
		if ( ! m_levelrunning ) return;
		
		if ( m_world.WorldShowExit ) {
			
			D.log("Exit Level");
			
			//GameObject.Find("PlayerExit").transform.localPosition = new Vector3(0,800,-1);
			GameObject _go = GameObject.Find("PlayerExit");
			MoveToRandom(_go, false);
			
			m_levelrunning = false;
			
			GameObject _enemy = GameObject.Find("Enemy");
			
			if ( _enemy != null ) {
				
				_enemy.SendMessage("SetDisabled", true);
			
				TweenParms parms = new TweenParms();
				parms.Prop("localPosition", new Vector3(_go.transform.position.x, _go.transform.position.y, _go.transform.position.z + 1)); // Position tween
				parms.Prop("rotation", new Vector3(0,0,360), true); // Rotation tween
				parms.Prop("localScale", new Vector3(.25f,.25f,.25f)); // Scale tween
				parms.Ease(EaseType.Linear); // Easing type
				parms.Loops(1);
				parms.OnComplete(DoEnemyComplete);
					
				HOTween.To(_enemy.transform, 6, parms );
			} 
			else {
				EnemyComplete();	
			}
		}
		
		if ( m_world.WorldCompleted ) {
			
		}
	}
	
	private void DoEnemyComplete( TweenEvent e) {
		EnemyComplete();		
	}
	
	private void EnemyComplete() {
		
		D.log("Complete");
		
		// destroy the world
		Destroy(GameObject.Find("Enemy"));

		GamePlayerController.Instance.Kill();
		
		// destroy the world
		Destroy(GameObject.Find("_World"),1 );
			
		StartCoroutine(NewLevel());
	}
	
	private IEnumerator NewLevel() {
		
		GameObject.Find("GuiDialogComplete").SendMessage("Show");
		yield return new WaitForSeconds(2);
		StartLevel();
		yield return new WaitForSeconds(2);
		GameObject.Find("GuiDialogComplete").SendMessage("Hide");
	}
	
	public void StartGame() {
		
		
		m_global = new GameGlobal();
		m_global.Level = StartingLevel;
		m_global.Stage = 1;
		m_global.Difficulty = StartingDifficulty;
		m_global.Score = 0;
		
		m_levelrunning = false;
		
		m_global.Lives = 3;
		GameLives.Instance.UpdateLives();
		
		// load hi score
		
		m_global.HiScore = PlayerPrefs.GetInt("hiscore",0);
		GameHiScore.Instance.UpdateScore();
		
		StartLevel();
	}
	
	public void StartLevel () {
		
		// build the next level
		
		m_global.Level++; // increase level
		GameLevel.Instance.UpdateValue();

		
		m_world = new GameWorld();
		
		m_world.WorldWidth = Mathf.RoundToInt ( 8 + ( m_global.Level * .15f ) + m_global.Difficulty );
		m_world.WorldDepth = Mathf.RoundToInt ( 6 + ( m_global.Level * .15f ) + m_global.Difficulty );
		
		m_world.WorldArea = m_world.WorldWidth * m_world.WorldDepth;
		m_world.WorldCleared = Mathf.RoundToInt ( 60 + ( ( m_global.Level * .25f ) + m_global.Difficulty ) );
		D.log("build");
		GameWorldBuilder.Instance.SetWorld( m_world );
		GameWorldBuilder.Instance.Build();
		
		m_global.Cleared = 0;
		
		DefineMinimals();
		DefineImpassables();
		DefineScenery();

		// reset some important level variables
		D.log("setup");
		
		m_world.WorldCompleted = false;
		m_world.WorldShowExit = false;

		// place the player
		GamePlayerController.Instance.CreatePlayer();
		
		// calculate enemy speed
		float _speed = 3.5f - ( ( m_global.Level * .15f ) - ( m_global.Difficulty * .10f ) );
		if ( _speed <= 0 ) _speed = .1f;
		
		D.log("enemy speed = " + _speed);
		
		int _numenemy = Mathf.RoundToInt ( 1 + ( ( m_global.Level * .035f ) + ( m_global.Difficulty * .05f ) ) );
		D.log("#enemy=" + _numenemy);
		for ( int i = 0; i < _numenemy; i++ ) {
			GameEnemyController.Instance.CreateEnemy(_speed);
		}
		
		GameObject.Find("GameCamera").SendMessage("Init");
		
		GameObject _go = (GameObject) LoadPrefab("Prefabs/Player/PlayerExit", "PlayerExit");
		//_go.transform.parent = Camera.mainCamera.transform;
		_go.transform.parent = GameObject.Find("_World").transform;
		// hide the playerexit
		GameObject.Find("PlayerExit").transform.localPosition = new Vector3(-1024,800,-100);
		
		GamePowerupController.Instance.Init(1,5,5);
		GamePowerupController.Instance.StartRunning();
		
		m_levelrunning = true;
	}
	
	public GameWorld World{
		get { return m_world; }
	}
	
	public GameGlobal Global {
		get { return m_global; }
	}
	
	public void AddScore(int score) {
		m_global.Score = m_global.Score + score;
		GameScore.Instance.SetScore(m_global.Score);
	}
	
	public void AddCleared(int cleared) {
		m_global.Cleared = m_global.Cleared + cleared;
		//D.log("cleared="+m_global.Cleared);
		//D.log("area="+m_world.WorldArea);
		GameCleared.Instance.SetCleared(m_global.Cleared);
		
	}
	
	/**
	 * 
	 * another minimal created by the enemy
	 * 
	 */
	
	public void MakeMinimal(int x, int y) {
		
		// make the terrain normal
		m_world.SetMinimal(x, y, true);
		
		// reset the terrain image
		GameWorldBuilder.Instance.SetTerrainGrass(x,y,"grass_0_m");
		AddCleared(-1);
	}

	/**
	 * 
	 * the minimal was destroyed by the player
	 */
	
	public void DestroyMinimal(int x, int y) {
		
		// make the terrain normal
		m_world.SetMinimal(x, y, false);
		
		// reset the terrain image
		GameWorldBuilder.Instance.SetTerrainGrass(x,y,"grass_0");
		
		// add to the score
		int _value = m_global.Difficulty;
		AddScore( _value );
		AddCleared(1);
	}
	
	/**
	 * 
	 * minimalised terrain is that which has had its magic 
	 * removed from mr minimal.
	 * 
	 * it is represented by the same artwork as before, except
	 * that there is no color shown
	 * 
	 */
	
	private void DefineMinimals() {
		
		// at the start we want at least 60% of the
		// playing field as minimal
		// 
		// the level will determine what % more of it
		// will be covered
		// 
		// the higher the level, the more covered
		//
		// at least 10% should be available for the
		// player
		//
		
		// get area
		
		int _area = m_world.WorldWidth * m_world.WorldDepth;
		int _min = Mathf.RoundToInt(  _area * .60f );
		int _num = _min + GameController.Instance.Global.Level;
		
		// set total cleared
		m_global.Cleared = _area;// - _num;
		
		// set minimals
		
		int i = 0;
		
		do {
			
			bool _ok = true;
		
			int _x = Random.Range( 0, m_world.WorldWidth );
			int _y = Random.Range( 0, m_world.WorldDepth );
			
			// make sure its not already occupied
			
			
			if ( m_world.IsMinimalTerrain(_x,_y) ) {
				_ok = false;
			}
			
			if ( ! m_world.IsPassableTerrain(_x,_y) ) {
				_ok = false;
			}
			
			if ( _ok ) {
				MakeMinimal(_x,_y);			
				i++;
			}
			
		} while ( i < _num );
		
	}
	
	public void MakeImpassable( int x, int y) {
		
		// make the terrain normal
		m_world.SetPassable(x, y, false);
		
		// reset the terrain image
		GameWorldBuilder.Instance.SetTerrainGrass(x,y,"grass_0_i");
		
		GameObject _prefab = LoadPrefab("Prefabs/Scenery/Impassable", "Impassable");
		GameSprite _sprite = (GameSprite) _prefab.GetComponentInChildren<GameSprite>();
		_sprite.MoveTo(new Vector2(x,y));
		AttachToWorld(_prefab);
	}
	
	public void DestroyImpassable( int x, int y) {
		
	}
	
	/**
	 * 	passable terrain is that which has nothing inside which
	 * 	can block another objects movement
	 *
	 *	for our purposes everyting is passable, and this routine
	 *	will randomly pick spots that are not passable
	 *
	 *	kind of the reverse
	 *
	 */
	
	private void DefineImpassables() {
		
		int _num = Mathf.RoundToInt ( 1 + ( GameController.Instance.Global.Level * .15f ) + ( m_global.Difficulty * .15f ) );
		
		int i = 0;
		
		do {
			
			bool _add = true;
			
			int _x = Random.Range( 0, m_world.WorldWidth );
			int _y = Random.Range( 0, m_world.WorldDepth );
			
			// make sure not already occupied
			
			if ( m_world.IsMinimalTerrain(_x,_y) ) {
				_add = false;
			}
			
			if ( ! m_world.IsPassableTerrain(_x,_y) ) {
				_add = false;
			}

			if ( _add ) {
				/*
				D.log(_x);
				D.log(_y);
				D.log(m_world.IsMinimalTerrain(_x,_y));
				D.log(m_world.IsPassableTerrain(_x,_y));
				D.log("Add Impassable");
				*/
				
				MakeImpassable(_x,_y);
				i++;
			}
			
		} while ( i < _num );
		
	}
	
	private void DefineScenery() {
		
		int _num = GameController.Instance.Global.Level;
		
		int i = 0;
		
		do {
			
			bool _add = true;
			
			int _x = Random.Range( 0, m_world.WorldWidth );
			int _y = Random.Range( 0, m_world.WorldDepth );
			
			// make sure not already occupied
			
			if ( m_world.IsMinimalTerrain(_x,_y) ) {
				_add = false;
			}
			
			if ( ! m_world.IsPassableTerrain(_x,_y) ) {
				_add = false;
			}

			if ( _add ) {
				
				MakeScenery(_x,_y);
				i++;
			}
		} while ( i < _num );
	}
		
	private void MakeScenery( int x, int y) {
		
		int _type = Random.Range(0, 4);
		
		switch ( _type ) {
		
			
		case 0:			// tree
			break;
		case 1:			// tall grass
			break;
		case 2:			//flower
			break;
		}
			
	}
	
	public static GameObject LoadPrefab(string prefab, string name) {
		
		GameObject _go = (GameObject) Object.Instantiate(Resources.Load(prefab));
		if ( name != null ) _go.name = name;
		return _go;
	}
	
	private void AttachToWorld(GameObject go) {
		GameObject _w = GameObject.Find("_World");
		go.transform.parent = _w.transform;
	}
	
	public void MoveToRandom(GameObject go, bool passable) {
	
		int _x = Random.Range( 0, m_world.WorldWidth );
		int _y = Random.Range( 0, m_world.WorldDepth );
				
		m_world.SetPassable(_x,_y, passable);
		
		GameSprite _gs = (GameSprite) go.GetComponentInChildren<GameSprite>();
		_gs.MoveTo(new Vector2(_x,_y));
	}
	
	public void GameOver() {
		
		D.log("Game Over");
		
		// hide the playerexit
		GameObject.Find("PlayerExit").transform.localPosition = new Vector3(-1024,800,-100);
		
		// destroy the world
		Destroy(GameObject.Find("Enemy"));

		GamePlayerController.Instance.Kill();
		
		// destroy the world
		Destroy(GameObject.Find("_World"),1 );
		
		GameObject.Find("GuiDialogGameOver").SendMessage("Show");
	}
	
	public void KillGame() {
		
		D.log("Kill Game");
		
		// hide the playerexit
		Destroy(GameObject.Find("PlayerExit"));
		
		// destroy the world
		Destroy(GameObject.Find("Enemy"));

		GamePlayerController.Instance.Kill();
		
		// destroy the world
		Destroy(GameObject.Find("_World"),1 );
		
		m_global.Lives = 0;
		m_global.Score = 0;
		m_global.Level = 0;
		
		GameLives.Instance.UpdateLives();
		GameLevel.Instance.UpdateValue();
		GameScore.Instance.SetScore(0);
		
	}
}
