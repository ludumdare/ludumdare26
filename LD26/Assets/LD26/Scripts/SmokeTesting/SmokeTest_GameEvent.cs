using UnityEngine;
using System.Collections;

public class SmokeTest_GameEvent : MonoBehaviour {
	
public class EventSmokeTest: GameEvent {

		public int EventValue;
}
	
	
	private int m_value;
	
	// Use this for initialization
	void OnEnable () {
		
		GameEvents.Instance.AddListener<EventSmokeTest>(DoSmokeTest);
	}
	
	void OnDisable () {
		
		GameEvents.Instance.RemoveListener<EventSmokeTest>(DoSmokeTest);
	}
	
	// Update is called once per frame
	void Update () {
		
		m_value = m_value + 1;
		EventSmokeTest e = new EventSmokeTest();
		e.EventValue = m_value;
		GameEvents.Instance.Raise(e);
	
	}
	
	void DoSmokeTest(EventSmokeTest e) {
		
		D.log("GameLogic", "SmokeTest " + e.EventValue);
		D.log("Some GameObject = " + gameObject);
	}
}
