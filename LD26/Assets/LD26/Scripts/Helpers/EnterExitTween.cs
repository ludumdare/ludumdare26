using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class EnterExitTween : MonoBehaviour {
	
	public Vector3		TweenEnter;
	public Vector3		TweenTo;
	public Vector3		TweenExit;
	public int			TweenSpeed;
	public int			AutoDelay = 1;

	
	void Awake() {
		// find the gui camera and attach
		//GameObject _gc = GameObject.Find("GuiCamera");
		//gameObject.transform.parent = _gc.transform;
	}
	
	// Use this for initialization
	void Start () {
		//Show ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void Hide() {
		HOTween.To(gameObject.transform, TweenSpeed, new TweenParms().Prop("localPosition", TweenExit) );
	}
	
	public void Show() {
		gameObject.transform.localPosition = TweenEnter;
		HOTween.To(gameObject.transform, TweenSpeed, new TweenParms().Prop("localPosition", TweenTo) );
	}
	
	public void AutoShow() {
		Show();		
		StartCoroutine(AutoHide());	
	}
	
	private IEnumerator AutoHide() {
		yield return new WaitForSeconds(AutoDelay);
		Hide();
	}

}
