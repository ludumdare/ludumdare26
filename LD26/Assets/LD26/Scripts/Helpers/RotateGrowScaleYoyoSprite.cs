using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class RotateGrowScaleYoyoSprite : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	TweenParms parms = new TweenParms();
	
	//parms.Prop("position", new Vector3(10,20,30)); // Position tween
	parms.Prop("rotation", new Vector3(0,0,360), true); // Rotation tween
	parms.Prop("localScale", new Vector3(2,2,2)); // Scale tween
	parms.Ease(EaseType.EaseOutBounce); // Easing type
	//parms.Delay(1); // Initial delay
	parms.Loops(-1, LoopType.YoyoInverse);
		
		
	HOTween.To(gameObject.transform, 2, parms );	
		
	}
	
}
