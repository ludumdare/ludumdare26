using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class GameStart : MonoBehaviour { 
	
	void Start() {
		
		
		HOTween.Init( true, false, false );
		GameObject.Find("HOTween").gameObject.layer = 31;
		GameObject.Find("GuiDialogStart").SendMessage("Show");
	}
}
