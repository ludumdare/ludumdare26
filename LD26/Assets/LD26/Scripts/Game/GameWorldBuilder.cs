using UnityEngine;
using System.Collections;

/**
 * 
 * Build a World based on World Properties
 * 
 * 
 * 
 */

public class GameWorldBuilder : MonoSingleton<GameWorldBuilder> {

	public float			SkyOffset;
	public float 			HillsOffset;
	
	private GameWorld		m_world;
	
	private GameObject		go_world;	// game world
	private GameObject		go_field;	// field of play
	private GameObject		go_scenery;	// scenery
	
	
	// Use this for initialization
	void Start () {
		DontDestroyOnLoad(gameObject);
	}
	
	// set the world properties
	
	public void SetWorld(GameWorld world) {
		
		m_world = world;
	}
	
	// build the world in the current scene
	
	public void Build() {
		
		// create empty placeholder objects
		
		go_world = new GameObject();
		go_world.name = "_World";
		
		// add sky
		GameObject _sky = (GameObject) Object.Instantiate(Resources.Load("Prefabs/Scenery/Sky"));
		_sky.transform.parent = go_world.transform;
		
		// add hills
		GameObject _hills = (GameObject) Object.Instantiate(Resources.Load("Prefabs/Scenery/Hills"));
		_hills.transform.parent = go_world.transform;
		
		go_field = new GameObject();
		go_field.name = "_Field";
		go_field.transform.parent = go_world.transform;
		
		go_scenery = new GameObject();
		go_scenery.name = "_Scenery";
		go_scenery.transform.parent = go_world.transform;
		
		// define world elements & initialize
		
		m_world.GameWorldTerrains = new GameWorldTerrain[ m_world.WorldWidth, m_world.WorldDepth ];	
		
		for ( int _x = 0; _x < m_world.WorldWidth; _x++ ) {
			for ( int _y = 0; _y < m_world.WorldDepth; _y++ ) {
				m_world.GameWorldTerrains[ _x, _y ] = new GameWorldTerrain();
			}
		}
		
		// get the grass prefab
		
		string _prefab = "Prefabs/Scenery/Grass";
		
		for ( int _x = 0; _x < m_world.WorldWidth; _x++ ) {
			
			for ( int _y = 0; _y < m_world.WorldDepth; _y++ ) {
			
				GameObject _grass = (GameObject) Object.Instantiate( Resources.Load( _prefab ) );
				_grass.transform.parent = go_field.transform;
				_grass.transform.localPosition = new Vector3( ( _x * 64f ), ( _y * 64f ), 0f );
				
				tk2dSprite _sprite = (tk2dSprite) _grass.GetComponentInChildren<tk2dSprite>();			
				
				string _spritename = "grass_0";
				_sprite.SetSprite(_spritename);
				
				m_world.GameWorldTerrains[ _x, _y ].TerrainObject = _grass;
			}
		}
		
		_sky.transform.localPosition = new Vector3( _sky.transform.localPosition.x, ( m_world.WorldDepth * 64 ) + SkyOffset ,_sky.transform.localPosition.z);
		_hills.transform.localPosition = new Vector3( _hills.transform.localPosition.x, ( m_world.WorldDepth * 64 ) + HillsOffset ,_hills.transform.localPosition.z);
	}
	
	public void SetTerrainGrass(int x, int y, string tile) {
		
		// get the terrain object
		GameObject _go = m_world.GetTerrainObject(x,y);
		tk2dSprite _sprite = (tk2dSprite) _go.GetComponentInChildren<tk2dSprite>();
		
		// set the sprite tile
		_sprite.SetSprite(tile);
		
		
	}
	
}
